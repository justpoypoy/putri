/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.5-10.1.21-MariaDB : Database - futsal
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`futsal` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `futsal`;

/*Table structure for table `about` */

DROP TABLE IF EXISTS `about`;

CREATE TABLE `about` (
  `id_about` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text NOT NULL,
  PRIMARY KEY (`id_about`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `about` */

insert  into `about`(`id_about`,`isi`) values (1,'<p>Halaman About 12</p>\r\n');

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `email` varchar(35) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id_admin`,`username`,`password`,`nama`,`email`) values (25,'admin','21232f297a57a5a743894a0e4a801fc3','Admin futsal','adminfutsal@gmail.com');

/*Table structure for table `data_lapangan` */

DROP TABLE IF EXISTS `data_lapangan`;

CREATE TABLE `data_lapangan` (
  `id_data_lapangan` int(11) NOT NULL AUTO_INCREMENT,
  `id_lapangan` int(11) DEFAULT NULL,
  `mulai` varchar(5) DEFAULT NULL,
  `selesai` varchar(5) DEFAULT NULL,
  `harga` float(14,2) DEFAULT NULL,
  PRIMARY KEY (`id_data_lapangan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `data_lapangan` */

insert  into `data_lapangan`(`id_data_lapangan`,`id_lapangan`,`mulai`,`selesai`,`harga`) values (1,3,'16:00','17:00',100000.00),(2,4,'12:00','13:00',150000.00),(3,5,'13:00','14:00',130000.00),(4,3,'15:00','16:00',90000.00);

/*Table structure for table `halaman` */

DROP TABLE IF EXISTS `halaman`;

CREATE TABLE `halaman` (
  `id_halaman` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text,
  PRIMARY KEY (`id_halaman`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `halaman` */

insert  into `halaman`(`id_halaman`,`isi`) values (1,'<p>Test</p>\r\n');

/*Table structure for table `ketentuan` */

DROP TABLE IF EXISTS `ketentuan`;

CREATE TABLE `ketentuan` (
  `id_ketentuan` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text,
  PRIMARY KEY (`id_ketentuan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ketentuan` */

insert  into `ketentuan`(`id_ketentuan`,`isi`) values (1,'<p>ketentuan 12</p>\r\n');

/*Table structure for table `lapangan` */

DROP TABLE IF EXISTS `lapangan`;

CREATE TABLE `lapangan` (
  `id_lapangan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lapangan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_lapangan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `lapangan` */

insert  into `lapangan`(`id_lapangan`,`nama_lapangan`) values (3,'Lapangan A'),(4,'Lapangan AB'),(5,'Lapangan B');

/*Table structure for table `pelanggan` */

DROP TABLE IF EXISTS `pelanggan`;

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(35) NOT NULL,
  `nama_klub` varchar(30) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varbinary(60) DEFAULT NULL,
  `no_telpon` char(12) NOT NULL,
  `alamat` text,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `pelanggan` */

insert  into `pelanggan`(`id_pelanggan`,`nama`,`nama_klub`,`email`,`password`,`no_telpon`,`alamat`) values (22,'suwondo','suwondo FC','me@suwondo.id','827ccb0eea8a706c4c34a16891f84e7b','081282102909','Jakarta Selatan'),(27,'popo','getme FC','justpoypoy@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','08998600083','Jakarta selatan'),(28,'Putri Tiara Gani','aseloleFC','putritiaragani14@gmail.com','281e5adc7896e59b5514bea4bb38abd8','081318865373',NULL),(29,'suwondo','suwondo FC','test@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','0128182198',NULL);

/*Table structure for table `pemesanan` */

DROP TABLE IF EXISTS `pemesanan`;

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_lapangan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tanggal_main` date DEFAULT NULL,
  `uniqid` int(5) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'book',
  `bank` varchar(10) DEFAULT NULL,
  `nama_akun` varchar(20) DEFAULT NULL,
  `jumlah_bayar` float(14,2) DEFAULT NULL,
  `gambar_upload` text,
  PRIMARY KEY (`id_pemesanan`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan` */

insert  into `pemesanan`(`id_pemesanan`,`id_data_lapangan`,`id_pelanggan`,`tanggal_main`,`uniqid`,`status`,`bank`,`nama_akun`,`jumlah_bayar`,`gambar_upload`) values (1,4,22,'2018-07-02',222,'lunas',NULL,NULL,NULL,NULL),(2,1,22,'2018-07-03',164,'expired',NULL,NULL,NULL,NULL),(3,1,22,'2018-07-05',171,'lunas','BCA','suwondo',100171.00,NULL),(4,4,28,'2018-07-04',27,'lunas','BCA','ciripa',90027.00,NULL),(5,2,28,'2018-07-06',883,'expired',NULL,NULL,NULL,NULL),(6,4,22,'2018-07-09',902,'lunas','bca','suwondo',90902.00,'55the-php-practitioner1.jpg'),(7,1,22,'2018-07-12',720,'lunas','BCA','Suwondo',100720.00,NULL),(8,4,22,'2018-07-12',114,'lunas','BCA','suwondo',90114.00,NULL),(9,3,22,'2018-07-10',19,'lunas','BCA','suwondoo',130019.00,NULL);

/*Table structure for table `profil` */

DROP TABLE IF EXISTS `profil`;

CREATE TABLE `profil` (
  `id_profil` int(11) NOT NULL AUTO_INCREMENT,
  `namafutsal` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `fax` char(20) NOT NULL,
  `no_hp` char(12) NOT NULL,
  PRIMARY KEY (`id_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `profil` */

insert  into `profil`(`id_profil`,`namafutsal`,`alamat`,`kodepos`,`fax`,`no_hp`) values (1,'Penyewaan Lapangan Futsal','Suryamandala, Bekasi, Indonesia.','45571','09828937823','07238472389');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
