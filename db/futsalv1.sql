/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.5-10.1.21-MariaDB : Database - futsal
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`futsal` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `futsal`;

/*Table structure for table `about` */

DROP TABLE IF EXISTS `about`;

CREATE TABLE `about` (
  `id_about` int(11) NOT NULL AUTO_INCREMENT,
  `isi` text NOT NULL,
  PRIMARY KEY (`id_about`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `about` */

insert  into `about`(`id_about`,`isi`) values (1,'<p><strong>Boyolali Futsal</strong></p>\r\n\r\n<p>Alamat &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Jl. Kemuning boyolali</p>\r\n\r\n<p>Kode Pos &nbsp; &nbsp; &nbsp; : 45571</p>\r\n\r\n<p>Fax/Telp. &nbsp; &nbsp; &nbsp; &nbsp;: 081874687243</p>\r\n'),(2,'<ul>\r\n	<li>Dilarang membuang puntung rokok yang masih menyala di sembarang tempat</li>\r\n	<li>Dilarang bermain api dan membawa bahan kimia yang membahayakan</li>\r\n	<li>Dilarang membawa minuman keras dan obat-obatan terlarang ke dalam kompleks</li>\r\n	<li>Dilarang mencoret dinding dan merusak fasilitas olah raga&nbsp;</li>\r\n	<li>Dilarang membawa senjata tajam atau senjata api kecuali yang bertugas khusus</li>\r\n	<li>Dilarang berbuat tindakan amoral (judi, asusila dan pornografi lainnya)</li>\r\n	<li>Dilarang membuang sampah atau sisa makanan di sembarang tempat</li>\r\n	<li>Seluruh pengunjung tetap bertanggung jawab terhadap bawaan barang pribadi kecuali dititipkan secara sah dan disertai surat tanda terima</li>\r\n	<li>Kehilangan barang berharga atau barang lainnya yang tidak dititipkan secara sah kepada pengelola yang bertugas di luar tanggung jawab kami</li>\r\n	<li>Untuk menghindari risiko kehilangan barang-barang berharga seperti uang dan perhiasan, sebaiknya tidak dititipkan dan disimpan/diamankan secara pribadi oleh pengunjung sendiri</li>\r\n	<li>Kenyamanan dan keamanan bersama selalu menjadi perhatian kami, namun sebaliknya koordinasi, laporan dan kritik atas segala keterbatasan dan kekurangan pelayanan kami menjadi harapan guna perbaikan selanjutnya.</li>\r\n</ul>\r\n');

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `email` varchar(35) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id_admin`,`username`,`password`,`nama`,`email`) values (25,'admin','21232f297a57a5a743894a0e4a801fc3','Admin futsal','adminfutsal@gmail.com');

/*Table structure for table `data_lapangan` */

DROP TABLE IF EXISTS `data_lapangan`;

CREATE TABLE `data_lapangan` (
  `id_data_lapangan` int(11) NOT NULL AUTO_INCREMENT,
  `id_lapangan` int(11) DEFAULT NULL,
  `mulai` varchar(5) DEFAULT NULL,
  `selesai` varchar(5) DEFAULT NULL,
  `harga` float(14,2) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'available',
  PRIMARY KEY (`id_data_lapangan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `data_lapangan` */

insert  into `data_lapangan`(`id_data_lapangan`,`id_lapangan`,`mulai`,`selesai`,`harga`,`status`) values (1,3,'16:00','17:00',100000.00,'available'),(2,4,'12:00','13:00',150000.00,'available'),(3,5,'13:00','14:00',130000.00,'available'),(4,3,'15:00','16:00',90000.00,'available');

/*Table structure for table `harga` */

DROP TABLE IF EXISTS `harga`;

CREATE TABLE `harga` (
  `id_harga` int(11) NOT NULL AUTO_INCREMENT,
  `waktu` varchar(40) NOT NULL,
  `harga` varchar(30) NOT NULL,
  PRIMARY KEY (`id_harga`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `harga` */

insert  into `harga`(`id_harga`,`waktu`,`harga`) values (1,'siang','80000'),(2,'malam','120000');

/*Table structure for table `hari` */

DROP TABLE IF EXISTS `hari`;

CREATE TABLE `hari` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nama_hari` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `hari` */

insert  into `hari`(`id`,`nama_hari`) values (1,'senin'),(2,'selasa'),(3,'rabu'),(4,'kamis'),(5,'jumat'),(6,'sabtu'),(7,'minggu');

/*Table structure for table `jadwal` */

DROP TABLE IF EXISTS `jadwal`;

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `jam` time NOT NULL,
  `id_harga` int(11) NOT NULL,
  `jams` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `jadwal` */

insert  into `jadwal`(`id_jadwal`,`jam`,`id_harga`,`jams`) values (1,'08:00:00',1,'08.00-09.00'),(3,'09:00:00',1,'09.00-10.00'),(4,'10:00:00',1,'10.00-11.00'),(5,'11:00:00',1,'11.00-12.00'),(6,'12:00:00',1,'12.00-13.00'),(7,'13:00:00',1,'13.00-14.00'),(8,'14:00:00',1,'14.00-15.00'),(9,'15:00:00',1,'15.00-16.00'),(10,'16:00:00',1,'16.00-17.00'),(11,'18:00:00',2,'18.00-19.00'),(12,'19:00:00',2,'19.00-20.00'),(13,'20:00:00',2,'20.00-21.00'),(14,'21:00:00',2,'21.00-22.00'),(15,'22:00:00',2,'22.00-23.00'),(16,'23:00:00',2,'23.00-24.00');

/*Table structure for table `lapangan` */

DROP TABLE IF EXISTS `lapangan`;

CREATE TABLE `lapangan` (
  `id_lapangan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lapangan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_lapangan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `lapangan` */

insert  into `lapangan`(`id_lapangan`,`nama_lapangan`) values (3,'Lapangan A'),(4,'Lapangan AB'),(5,'Lapangan B');

/*Table structure for table `pelanggan` */

DROP TABLE IF EXISTS `pelanggan`;

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(35) NOT NULL,
  `nama_klub` varchar(30) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varbinary(60) DEFAULT NULL,
  `no_telpon` char(12) NOT NULL,
  `alamat` text,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `pelanggan` */

insert  into `pelanggan`(`id_pelanggan`,`nama`,`nama_klub`,`email`,`password`,`no_telpon`,`alamat`) values (19,'Rojak Surdiyana','PSBS FC','rojaksurdiyana@live.com',NULL,'085794212222',NULL),(20,'Yana Hadiningrat','Pesantren FC','yanahadiningrat14@gmail.com',NULL,'087745565579',NULL),(21,'mulyadi sumirat','Pangumbahan FC','mulyadisumingrat@ymail.com',NULL,'08565987456',NULL),(22,'suwondo','suwondo FC','me@suwondo.id','827ccb0eea8a706c4c34a16891f84e7b','081282102909',NULL),(27,'popo','getme FC','justpoypoy@gmail.com','827ccb0eea8a706c4c34a16891f84e7b','08998600083','Jakarta selatan'),(28,'Putri Tiara Gani','aseloleFC','putritiaragani14@gmail.com','281e5adc7896e59b5514bea4bb38abd8','081318865373',NULL);

/*Table structure for table `pelanggan_asli` */

DROP TABLE IF EXISTS `pelanggan_asli`;

CREATE TABLE `pelanggan_asli` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `nama_klub` varchar(30) NOT NULL,
  `email` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `no_telpon` char(12) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `pelanggan_asli` */

insert  into `pelanggan_asli`(`id_pelanggan`,`username`,`password`,`nama`,`nama_klub`,`email`,`alamat`,`no_telpon`) values (15,'user','ee11cbb19052e40b07aac0ca060c23ee','user','test fc','testfc@yahoo.com','Desa Garawangi No. 195','021547896'),(19,'rojak','944b2a47cfbde4eb5144d3b9a00063e4','Rojak Surdiyana','PSBS FC','rojaksurdiyana@live.com','Desa Margamukti RT.001/RW.005 ','085794212222'),(20,'yana','e1ce1e8d0877b06b55b613d5b22b0251','Yana Hadiningrat','Pesantren FC','yanahadiningrat14@gmail.com','Ds Cineumbeuy No 178 RT06/RW02','087745565579'),(21,'mulyadi','ef34c0dc47139e55c72bb333c3810131','mulyadi sumirat','Pangumbahan FC','mulyadisumingrat@ymail.com','Ds Cinagara RT08/RW05','08565987456');

/*Table structure for table `pemesanan` */

DROP TABLE IF EXISTS `pemesanan`;

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_data_lapangan` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `tanggal_main` date DEFAULT NULL,
  `uniqid` int(5) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'book',
  `bank` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_pemesanan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan` */

insert  into `pemesanan`(`id_pemesanan`,`id_data_lapangan`,`id_pelanggan`,`tanggal_main`,`uniqid`,`status`,`bank`) values (1,4,27,'2018-07-01',6,'expired',NULL),(2,4,28,'2018-07-10',482,'book',NULL);

/*Table structure for table `pemesanan_asli` */

DROP TABLE IF EXISTS `pemesanan_asli`;

CREATE TABLE `pemesanan_asli` (
  `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `id_harga` int(11) NOT NULL,
  `id_jadwal` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama_klub` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `no_telpon` char(12) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` varchar(100) NOT NULL,
  `harga` char(10) NOT NULL,
  `dp` char(10) NOT NULL,
  `sisa` char(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id_pemesanan`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan_asli` */

insert  into `pemesanan_asli`(`id_pemesanan`,`id_admin`,`id_pelanggan`,`id_harga`,`id_jadwal`,`username`,`nama_klub`,`alamat`,`no_telpon`,`tanggal`,`jam`,`harga`,`dp`,`sisa`,`status`) values (44,0,22,2,11,'user','test fc','Desa Garawangi No. 195','021547896','2016-08-11','18.00-19.00','120000','0','0','Lunas'),(45,0,22,2,12,'user','test fc','Desa Garawangi No. 195','021547896','2016-08-11','19.00-20.00','120000','0','0','Lunas'),(46,25,0,2,13,'mulyadi','Pangumbahan FC','Ds Cinagara RT08/RW05','08565987456','2016-08-11','20.00-21.00','120000','0','','Lunas'),(47,0,22,1,1,'user','test fc','Desa Garawangi No. 195','021547896','2016-09-02','08.00-09.00','80000','0','0','Lunas'),(48,0,22,1,1,'user','test fc','Desa Garawangi No. 195','021547896','2016-09-04','08.00-09.00','80000','0','0','Lunas'),(49,0,22,1,1,'user','test fc','Desa Garawangi No. 195','021547896','2016-11-01','08.00-09.00','80000','0','0','Lunas'),(50,25,0,1,1,'user','test fc','Desa Garawangi No. 195','021547896','2016-11-24','08.00-09.00','80000','0','0','Lunas'),(51,0,22,1,1,'user','test fc','Desa Garawangi No. 195','021547896','2016-11-25','08.00-09.00','80000','0','0','Lunas'),(52,0,22,1,1,'user','test fc','Desa Garawangi No. 195','021547896','2017-08-31','08.00-09.00','80000','','','Pending');

/*Table structure for table `profil` */

DROP TABLE IF EXISTS `profil`;

CREATE TABLE `profil` (
  `id_profil` int(11) NOT NULL AUTO_INCREMENT,
  `namafutsal` varchar(35) NOT NULL,
  `alamat` text NOT NULL,
  `kodepos` varchar(5) NOT NULL,
  `fax` char(20) NOT NULL,
  `no_hp` char(12) NOT NULL,
  PRIMARY KEY (`id_profil`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `profil` */

insert  into `profil`(`id_profil`,`namafutsal`,`alamat`,`kodepos`,`fax`,`no_hp`) values (1,'Penyewaan Lapangan Futsal','Suryamandala, Bekasi, Indonesia.','45571','09828937823','07238472389');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
