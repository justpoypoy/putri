<div class="col-md-4 col-md-offset-3">
  <div class="widget">
    <div class="widget-content">
		<form action="modul/aksi_login.php" method="post">
			<h1 class="text-center">Halaman Login</h1>
			<hr>
			<?php if(!empty($_GET['err'])){ ?>		
				<div class="alert alert-danger" role="alert">
					<span class="icon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>
					Enter a valid username and password
				</div>
			<?php } ?>
			<div class="login-fields">
				<div class="field">
					<label for="">Username</label>
					<input type="text" id="" name="email" value="" placeholder="Your Email" class="login username-field" required />
				</div> 
				<div class="field">
					<label for="password">Password</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" required/>
				</div>
			</div>
			<div class="login-actions">
				<input type="submit" name="submit" class="button btn btn-success"/>
				<a href="index.php?modul=register" class="button btn btn-info pull-right">Register Here!</a>
			</div> <!-- .actions -->
		</form>
    </div> <!-- /widget-content -->
  </div> <!-- /widget -->  
</div> <!-- /col-md-3 -->
<?php
include 'modul/menu_kanan.php';
?>