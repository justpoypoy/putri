<script type="text/javascript" language="JavaScript">
  $(document).ready(function() {
    $('#pesan').DataTable( {
      "searching":false,
      "paging":   false,
      "ordering": false,
      "info":     false
    } );
  } );

  function konfirmasi()
  {
    tanya = confirm("Anda yakin akan menghapus data ?");
    if (tanya== true) return true;
    else return false; 
  } 
</script>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">         
       <div class="span12">
        <legend>
          <b>Daftar Booking</b>
        </legend>
        <table id="pesan" class="display">
          <thead>
            <tr>
              <th>No.</th> 
              <th>Nama Klub</th> 
              <th>Tanggal main</th>
              <th>Jam main</th>
              <th>Status</th>
              <th>AKSI</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $sql=mysql_query("SELECT a.id_data_lapangan, c.id_pemesanan, a.mulai, a.selesai, b.nama_lapangan, c.tanggal_main, d.no_telpon, d.nama_klub, d.email, c.status
              FROM data_lapangan a
              LEFT JOIN lapangan b ON b.id_lapangan = a.id_lapangan
              LEFT JOIN pemesanan c ON c.id_data_lapangan = a.id_data_lapangan
              LEFT JOIN pelanggan d ON d.id_pelanggan = c.id_pelanggan
              WHERE c.id_pelanggan = '$_SESSION[userid]'
              -- GROUP BY a.id_data_lapangan 
              ORDER BY c.tanggal_main DESC ");
            while($cc = mysql_fetch_array($sql)){
              $no++;
              ?>
              <tr>
                <td><?php echo $no;  ?></td>
                <td><?php echo $cc['nama_klub']; ?></td>
                <td><?php echo $cc['tanggal_main']; ?></td>
                <td><?php echo $cc['mulai'] . ' s/d ' . $cc['selesai']; ?></td>
                <td>
                  <?php if(($cc['status']=='book')){ ?>
                    <span class="label label-warning">
                      booked
                    </span>
                  <?php }
                  if(($cc['status']=='lunas')){ ?>
                    <span class="label label-success">
                      <?php echo $cc['status']; ?>
                    </span>
                  <?php }
                  if(($cc['status']=='expired')){ ?>
                    <span class="label label-danger">
                      <?php echo $cc['status']; ?>
                    </span>
                  <?php }
                  if(($cc['status']=='konfirmasi')){ ?>
                    <span class="label label-info">
                      Menunggu Konfirmasi
                    </span>
                  <?php }
                  if(($cc['status']=='play')){ ?>
                    <span class="btn btn-success btn-sm" rel="popover" id='el3' data-content="silahkan tunggu kurang lebih 10-15 menit sampai admin menkonfirmasi pesanan anda" title="info">
                      <?php echo $cc['status']; ?>
                    </span>
                  <?php } ?>
                </td>
                <td>
                  <?php if ($cc['status'] == 'book') {?>
                    <a href="?modul=bayar&id=<?php echo $cc['id_pemesanan'] ?>&user=<?php echo $cc['email'] ?>" class="btn btn-warning">Bayar</a>
                  <?php } ?>
                  <?php if(($cc['status']=='expired')){ ?>
                    <span class="label label-danger">
                      <?php echo $cc['status']; ?>
                    </span>
                  <?php } ?>
                  <?php if(($cc['status']=='konfirmasi')){ ?>
                    <span class="label label-info">
                      Menunggu Konfirmasi
                    </span>
                  <?php } ?>
                  <?php if(($cc['status']=='lunas')){ ?>
                    <span class="label label-success">
                      <?php echo $cc['status']; ?>
                    </span>
                  <?php } ?>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div><!--/span9-->
    </div><!--/row-->
  </div> <!--/container-->
</div> <!-- /main-inner -->
</div> <!--/ main-->
