<?php 
error_reporting(E_ALL);
$id = @$_GET['id'];
$userid = $_SESSION['userid'];
$data = mysql_query("SELECT data_lapangan.id_data_lapangan, lapangan.nama_lapangan, data_lapangan.mulai, data_lapangan.selesai, data_lapangan.harga FROM data_lapangan, lapangan WHERE data_lapangan.id_lapangan = lapangan.id_lapangan AND id_data_lapangan = '$id'");
$datauser = mysql_query("SELECT id_pelanggan, nama, nama_klub, email, no_telpon FROM pelanggan WHERE id_pelanggan = '$userid'");
$row = mysql_fetch_row($data);
$rowuser = mysql_fetch_row($datauser);
$rows = mysql_num_rows($data);
?>

<?php if(empty($id) || $rows == 0) { ?>
	<div class="col-md-9" >
		<div class="widget">
			<div class="widget-content">
				<legend align="center">
					<b>Info Lapangan</b>
				</legend>
				<div class="post">
					<div class="entry">
						<div id='isi'>
							<p>
								Anda harus melakukan pemilihan lapangan dahulu.
								<a href="?modul=lapangan">Klik Disini</a> untuk booking
							</p>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }else{ ?>

	<form method="POST" action="?modul=aksi_book" autocomplete="off">
		<div class="col-md-4" >
			<div class="widget">
				<div class="widget-content">
					<legend align="center">
						<b>Info Lapangan</b>
					</legend>
					<div class="post">
						<div class="entry">
							<div id='isi'>
								<input type="hidden" name="jam_mulai_main" value="<?php echo $row[2] ?>" >
								<input type="hidden" name="jam_selesai_main" value="<?php echo $row[3] ?>" >
								<input type="hidden" name="idlapangan" value="<?php echo $id; ?>">
								<label class="control-label" ><b>Nama Lapangan</b></label>
								<input name="nama" class="form-control" value="<?php echo $row[1]; ?>" readonly type="text">
								<label class="control-label" ><b>Jam Main</b></label>
								<input name="jam" class="form-control" value="<?php echo $row[2] . ' s/d ' . $row[3]; ?>" readonly type="text">
								<label class="control-label" ><b>Harga Lapangan</b></label>
								<input name="harga" class="form-control" value="Rp. <?php echo number_format($row[4]); ?>" readonly type="text">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5" >
			<div class="widget">
				<div class="widget-content">
					<legend align="center">
						<b>Info Pemesan</b>
					</legend>
					<div class="post">
						<div class="entry">
							<div id='isi'>
								<input type="hidden" name="idpelanggan" value="<?php echo $rowuser[0]; ?>">
								<label class="control-label" ><b>Tanggal Main</b></label>
								<input type="text" class="form-control" id="datepicker" name="tgl" required="required">
								<label class="control-label" ><b>Nama Pemesan</b></label>
								<input name="pemesan" class="form-control" value="<?php echo $rowuser[1]; ?>" readonly type="text">
								<label class="control-label" ><b>Nama Klub</b></label>
								<input name="klub" class="form-control" value="<?php echo $rowuser[2]; ?>" type="text" required>
								<label class="control-label" ><b>Email</b></label>
								<input name="email" class="form-control" value="<?php echo $rowuser[3]; ?>" readonly type="text">
								<label class="control-label" ><b>Kontak Person</b></label>
								<input name="kontak" class="form-control" value="<?php echo $rowuser[4]; ?>" type="text" required>
								<input type="submit" name="submit" class="btn btn-primary" value="Book Now">
								<a href="?modul=lapangan" class="btn btn-warning pull-right">Cancel</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" >
			<div class="widget">
				<div class="widget-content">
					<legend align="center">
						<b>Info Pembayaran</b>
					</legend>
					<div class="post">
						<div class="entry">
							<div id='isi'>
								<label class="control-label" ><b>Bank</b></label>
								<input class="form-control" value="BRI" readonly type="text">
								<label class="control-label" ><b>Nomor Rekening</b></label>
								<input class="form-control" value="034001056875502" readonly type="text">
								<label class="control-label" ><b>Rekening Atas Nama</b></label>
								<input class="form-control" value="Putri Tiara Gani" readonly type="text">
							</div>
						</div>
					</div>
					<img src="img/logo-bank.png" width="240" height="150">
				</div>
			</div>
		</div>
	</form>

<?php } ?>
