<?php error_reporting(E_ALL); ?>
<div class="col-md-9" >
	<div class="widget">
		<div class="widget-content">
			<legend align="center">
				<b>Info Lapangan</b>
			</legend>
			<div class="post">
				<div class="entry">
					<div id='isi'>

						<?php
						$now = date('Y-m-d');
						$data = mysql_query("SELECT a.id_data_lapangan, c.id_pemesanan, a.harga, a.mulai, a.selesai, c.status, b.nama_lapangan,
							c.tanggal_main
							FROM data_lapangan a
							LEFT JOIN lapangan b ON b.id_lapangan = a.id_lapangan
							LEFT JOIN pemesanan c ON c.id_data_lapangan = a.id_data_lapangan
							LEFT JOIN pelanggan d ON d.id_pelanggan = c.id_pelanggan
							GROUP BY a.id_data_lapangan
							ORDER BY b.nama_lapangan, a.mulai, a.selesai ASC"); 
						?>
							
								<?php
								$arr1 = array();
								$arr2 = array();
								$arr3 = array();
								$arr4 = array();
								$arr5 = array();
								$arr6 = array();
								$arr7 = array();
								$arr8 = array();

								while ($row = mysql_fetch_array($data)) {
									array_push($arr2, $row['nama_lapangan']);
									array_push($arr3, $row['mulai'] . ' s/d ' . $row['selesai']);
									array_push($arr4, 'Rp. ' . number_format($row['harga'], 2));
									array_push($arr5, $row['status']);
									array_push($arr6, $row['tanggal_main']);
									array_push($arr7, $row['id_data_lapangan']);
									array_push($arr8, $row['id_pemesanan']);
									if (!isset($arr1[$row['nama_lapangan']])) {
										$arr1[$row['nama_lapangan']]['rowspan'] = 0;
									}
										$arr1[$row['nama_lapangan']]['printed'] = 'no';
	            						$arr1[$row['nama_lapangan']]['rowspan'] += 1;
	            					}
								?>
								
								<table cellspacing='0' cellpadding='0' class="table">
									<tr>
										<th>Nama Lapangan</th>
										<th>Jam Main</th>
										<th>Harga</th>
										<th>Status</th>
										<?php if (!empty($_SESSION['usernamepengunjung']) && !empty($_SESSION['namapengunjung'])) { ?>
											<th>Aksi</th>
										<?php } ?>
									</tr>
									<?php 
									for($i=0; $i < sizeof($arr3); $i++) {
							            $Name = $arr2[$i];
							            echo "<tr>";
							            if ($arr1[$Name]['printed'] == 'no') {
							                echo "<td style='vertical-align : middle;' rowspan='".$arr1[$Name]['rowspan']."'>".$Name."</td>";
							                $arr1[$Name]['printed'] = 'yes';
							            }
							            echo "<td>".$arr3[$i]."</td>";
							            echo "<td>".$arr4[$i]."</td>";
							        ?>
							            <td>
							            	<?php 
							            	if ($arr5[$i] == 'book' && $now > $arr6[$i]) {
												mysql_query("UPDATE pemesanan SET status = 'expired' WHERE id_pemesanan = '$arr8[$i]'");
							            	}
							            	if ($arr5[$i] == 'book') {
												echo '<span class="label label-warning">Booked</span>';
											}else{
												echo '<span class="label label-info">Available</span>';
											} ?>
							            </td>
							            <?php 
							            	if (!empty($_SESSION['usernamepengunjung']) 
							            		&& !empty($_SESSION['namapengunjung'])) {
										echo '<td>';
											if ($arr5[$i] == 'book' && $arr6[$i] == $now){
												echo '<span class="label label-warning">Booked</span>';
											}else{
												echo '<a href="?modul=book&id='.$arr7[$i].'" class="btn btn-primary">Book Now</a>';
											}
										echo '</td>';
										} ?>
						            </tr>
						        <?php } ?>
								</table>
						</div>
					</div>
				</div>
			</div> <!-- /widget-content -->
		</div> <!-- /widget -->
	</div> <!-- /span6 -->

	<!-- menu kanan -->
	<?php include 'modul/menu_kanan.php'; ?>
