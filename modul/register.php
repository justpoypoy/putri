<div class="col-md-4 col-md-offset-3">
  <div class="widget">
    <div class="widget-content">
		<form action="modul/aksi_register.php" method="post">
			<h1 class="text-center">Halaman Register</h1>
			<hr>
			<?php if($_GET['err'] == 'yes'){ ?>		
				<div class="alert alert-danger" role="alert">
					<span class="icon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>
					Enter a valid username and password
				</div>
			<?php }
			if ($_GET['err'] == 'no'){ ?>
				<div class="alert alert-danger" role="alert">
					<span class="icon-check" aria-hidden="true"></span>
					<span class="sr-only">Success:</span>
					Thank you for register. Please refer to page login.
				</div>
			<?php }
			if ($_GET['err'] == 'mail'){ ?>
				<div class="alert alert-danger" role="alert">
					<span class="icon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>
					Please signin, your email already in system.
				</div>
			<?php } ?>
			<div class="register login-fields">
				<div class="field">
					<label for="">Email</label>
					<input type="email" id="" name="email" value="" placeholder="Your Email" class="login username-field" required />
				</div>
				<div class="field">
					<label for="name">Nama Lengkap</label>
					<input type="text" name="name" placeholder="Your Name" class="login username-field">
				</div>
				<div class="field">
					<label for="name">Nama Klub</label>
					<input type="text" name="name_club" placeholder="Your Name Club" class="login username-field">
				</div>
				<div class="field">
					<label for="name">Kontak</label>
					<input type="number" name="kontak" placeholder="Your Phone Number" class="login username-field">
				</div>
				<div class="field">
					<label for="password">Password</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" required/>
				</div>
			</div>
			<div class="login-actions">
				<input type="submit" name="submit" class="button btn btn-success"/>
				<a href="index.php?modul=login" class="button btn btn-info pull-right">Have account? SignIn Here!</a>
			</div> <!-- .actions -->
		</form>
    </div> <!-- /widget-content -->
  </div> <!-- /widget -->  
</div> <!-- /col-md-3 -->
<?php
include 'modul/menu_kanan.php';
?>