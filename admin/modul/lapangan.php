<script type="text/javascript" language="JavaScript">
	function konfirmasi()
	{
		tanya = confirm("Anda yakin akan menghapus data ?");
		if (tanya) {
			return true;
		}
		return false; 
	}
</script>
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">         
				<div class="span3">
					<?php 
					switch (isset($_GET['act'])) {
						default:
						?>
						<legend>
							<b>Tambah Master Data Lapangan</b>
						</legend>
						<form method=POST action="index.php?modul=aksi_lapangan&act=input_lapangan"'>
							<label class="control-label">
								<b>Nama Lapangan</b>
							</label>
							<input name="nama" class="input-large" type="text"  required>
							<input type="submit" class="btn btn-primary">
						</form>
						<?php 
						break;
						case "edit":
						$data=mysql_fetch_array(mysql_query("SELECT * FROM lapangan WHERE id_lapangan='$_GET[id]'"));	?>                     

						<legend>
							<b>Edit Master Data Lapangan</b>
						</legend> 
						<form method=POST action="index.php?modul=aksi_lapangan&act=update_lapangan">
							<input name="kode" class="input-large" value="<?php echo $data['id_lapangan']; ?>" type="hidden"  readonly>

							<label class="control-label" ><b>Nama Lapangan</b></label>
							<input name="nama" class="input-large" value="<?php echo $data['nama_lapangan']; ?>" type="text">
							<input type="submit" class="btn btn-primary">
						</form>
						<?php 
					}
					?> 
				</div>
				<div class="span9">
					<legend><b>Master Data Lapangan</b></legend>
					<table id="datatables" class="display">
						<thead>
							<tr>
								<th>No.</th> 
								<th>Nama</th>
								<th width="110px">AKSI</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$sql=mysql_query("SELECT * FROM lapangan ORDER BY id_lapangan DESC");
							$no=0;
							while($baris=mysql_fetch_array($sql)){
								$no++;
								?>
								<tr>
									<td align="center"><?php echo $no;  ?></td>
									<td align="center"><?php echo $baris['nama_lapangan']; ?></td>
									<td>
										<a href="index.php?modul=lapangan&act=edit&id=<?php echo $baris['id_lapangan']; ?>" class="btn btn-warning btn-sm" >Edit</a>
										<a href="index.php?modul=aksi_lapangan&act=hapus_lapangan&id=<?php echo $baris['id_lapangan']; ?>" onclick="return konfirmasi()" class="btn btn-info btn-sm" >Hapus</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
