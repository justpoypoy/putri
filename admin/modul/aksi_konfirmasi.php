

<?php 
$id = $_GET['id'];
$query = "SELECT a.nama_klub 
FROM pelanggan a 
JOIN pemesanan b 
ON b.id_pelanggan = a.id_pelanggan 
WHERE b.id_pemesanan = '$id' ";
$result = mysql_query($query);
$row = mysql_fetch_row($result);

$q = "SELECT a.id_pemesanan, a.jumlah_bayar, a.gambar_upload,
a.status, a.bank, a.tanggal_main, c.nama_lapangan, d.nama_klub, a.nama_akun, CONCAT(b.mulai, ' s/d ', b.selesai) jam_main
FROM pemesanan a
JOIN data_lapangan b ON a.id_data_lapangan = b.id_data_lapangan
JOIN lapangan c ON b.id_lapangan = c.id_lapangan
JOIN pelanggan d ON d.id_pelanggan = a.id_pelanggan
WHERE a.id_pemesanan = '$id'";
$data = mysql_query($q);
$fetch = mysql_fetch_row($data);
?>
<form method="POST" action="index.php?modul=aksi_konfirmasi_book" autocomplete="off">
	<div class="span4" >
		<div class="widget">
			<div class="widget-content">
				<legend align="center">
					<b>Info Akun Bank <i><?php echo $row[0]; ?></i></b>
				</legend>
				<div class="post">
					<div class="entry">
						<div id='isi'>
							<input type="hidden" name="id" value="<?php echo $id; ?>">
							<label class="control-label" ><b>Nama Akun Bank</b></label>
							<input class="form-control" value="<?php echo $fetch[8]; ?>" readonly type="text">
							<label class="control-label" ><b>Bank Akun</b></label>
							<input class="form-control" value="<?php echo $fetch[4]; ?>" readonly type="text">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span4" >
		<div class="widget">
			<div class="widget-content">
				<legend align="center">
					<b>Info Pembayaran <i><?php echo $row[0]; ?></i></b>
				</legend>
				<div class="post">
					<div class="entry">
						<div id='isi'>
							<label class="control-label" ><b>Jumlah Pembayaran</b></label>
							<input type="text" class="form-control" value="Rp. <?php echo number_format($fetch[1]) ?> " disabled>
							<label class="control-label" ><b>Foto Upload pembayaran</b></label>
							<img src="../Gambar/Gambar_bukti/<?php echo $fetch[2] ?>">
							<hr/>
							<input type="submit" name="submit" class="btn btn-primary" onclick="return konfirmasi('<?php echo $row[0] ?>')" value="Konfirmasi">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="span4" >
		<div class="widget">
			<div class="widget-content">
				<legend align="center">
					<b>Data Booking <i><?php echo $row[0]; ?></i></b>
				</legend>
				<div class="post">
					<div class="entry">
						<div id='isi'>
							<label class="control-label" ><b>Nama Lapangan</b></label>
							<input class="form-control" value="<?php echo $fetch[6] ?>" readonly type="text">
							<label class="control-label" ><b>Nama Klub</b></label>
							<input class="form-control" value="<?php echo $fetch[7] ?>" readonly type="text">
							<label class="control-label" ><b>Tanggal Main</b></label>
							<input class="form-control" value="<?php echo $fetch[5] ?>" readonly type="text">
							<label class="control-label" ><b>Jam Main</b></label>
							<input class="form-control" value="<?php echo $fetch[9] ?>" readonly type="text">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript" language="JavaScript">
function konfirmasi(nama)
{
    tanya = confirm("Anda yakin akan mengkonfirmasi pembayaran " + nama.toUpperCase() + " ini ?");
    if (tanya== true) return true;
    else return false; 
}
</script>