<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">         
        <div class="span12">
          <legend>
            <b>Daftar Pemesanan</b>
          </legend>
          <table id="datatables" class="display">
            <thead>
              <tr>
                <th>No.</th> 
                <th>Nama</th>
                <th>Nama Klub</th> 
                <th>Tanggal main</th>
                <th>Jam Main</th>
                <th>No Telpon</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              session_start();
              $sql=mysql_query("SELECT a.id_pelanggan, b.id_pemesanan, c.id_data_lapangan, a.nama, a.nama_klub, b.tanggal_main, CONCAT(c.mulai,' s/d ', c.selesai) AS jam_main, a.no_telpon, b.status
                FROM pelanggan a
                JOIN pemesanan b ON a.id_pelanggan = b.id_pelanggan
                JOIN data_lapangan c ON c.id_data_lapangan = b.id_data_lapangan
                GROUP BY b.id_pemesanan
                ORDER BY b.tanggal_main ASC ");
              while($cc=mysql_fetch_array($sql)){
                $no++;
                ?>
                <tr>
                  <td align="center"><?php echo $no;  ?></td>
                  <td align="center"><?php echo $cc['nama']; ?></td>
                  <td align="center"><?php echo $cc['nama_klub']; ?></td>
                  <td align="center"><?php echo $cc['tanggal_main']; ?></td>
                  <td align="center"><?php echo $cc['jam_main']; ?></td>
                  <td align="center"><?php echo $cc['no_telpon']; ?></td>
                  <td align="center">
                    <span class="label label-default"><?php echo $cc['status']; ?></span>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div><!--/span9-->
      </div><!--/row-->
    </div> <!--/container-->
  </div> <!-- /main-inner -->
</div> <!--/ main-->
