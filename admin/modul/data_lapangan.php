<script type="text/javascript" language="JavaScript">
	function konfirmasi()
	{
		tanya = confirm("Anda yakin akan menghapus data ?");
		if (tanya) {
			return true;
		}
		return false; 
	}
</script>
<div class="main">
	<div class="main-inner">
		<div class="container">
			<div class="row">         
				<div class="span3">
					<?php 
					switch (isset($_GET['act'])) {
						default:
						?>
						<legend>
							<b>Tambah Data Lapangan</b>
						</legend>
						<form method=POST action="index.php?modul=aksi_data_lapangan&act=input_data_lapangan"'>
							<label class="control-label">
								<b>Nama Lapangan</b>
							</label>
							<?php 
							$query = "SELECT * FROM lapangan";
							$aksi = mysql_query($query);
							echo '<select name="nama" required>';
							echo '<option value="">--pilih lapangan--</option>';
							while ($q = mysql_fetch_array($aksi)) {
								echo "<option value='$q[id_lapangan]''>$q[nama_lapangan]</option>";
							}
							echo '</select>';
							?>
							<label class="control-label">
								<b>Waktu Mulai</b>
							</label>
							<input name="mulai" class="input-large" type="text" placeholder="contoh: 15:00" maxlength="5" required>
							<label class="control-label">
								<b>Waktu Selesai</b>
							</label>
							<input name="selesai" class="input-large" type="text" placeholder="contoh: 16:00" maxlength="5" required>
							<label class="control-label">
								<b>Harga</b>
							</label>
							<input name="harga" class="input-large" type="number" required>
							<input type="submit" class="btn btn-primary">
						</form>
						<?php 
						break;
						case "edit":
						$data=mysql_fetch_array(mysql_query("SELECT * FROM lapangan WHERE id_lapangan='$_GET[id]'"));	?>                     

						<legend>
							<b>Edit Data Lapangan</b>
						</legend> 
						<form method=POST action="index.php?modul=aksi_lapangan&act=update_lapangan">
							<input name="kode" class="input-large" value="<?php echo $data['id_lapangan']; ?>" type="hidden"  readonly>

							<label class="control-label" ><b>Nama Lapangan</b></label>
							<input name="nama" class="input-large" value="<?php echo $data['nama_lapangan']; ?>" type="text">
							<input type="submit" class="btn btn-primary">
						</form>
						<?php 
					}
					?> 
				</div>
				<div class="span9">
					<legend><b>Data Lapangan</b></legend>
					<table id="datatables" class="display">
						<thead>
							<tr>
								<th>No.</th> 
								<th>Nama</th>
								<th>Mulai s/d Selesai</th>
								<th>Harga</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$sql=mysql_query("SELECT data_lapangan.id_data_lapangan, lapangan.nama_lapangan, data_lapangan.mulai, data_lapangan.selesai, data_lapangan.harga
								FROM data_lapangan, lapangan WHERE lapangan.id_lapangan = data_lapangan.id_lapangan");
							$no=0;
							while($baris=mysql_fetch_array($sql)){
								$no++;
								?>
								<tr>
									<td align="center"><?php echo $no;  ?></td>
									<td align="center"><?php echo $baris['nama_lapangan']; ?></td>
									<td align="center"><?php echo $baris['mulai'] . ' s/d ' . $baris['selesai']; ?></td>
									<td align="center">Rp. <?php echo number_format($baris['harga']); ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
